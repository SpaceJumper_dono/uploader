import com.squareup.okhttp.RequestBody;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.ProgressListener;
import com.yandex.disk.rest.ResourcesArgs;
import com.yandex.disk.rest.RestClient;
import com.yandex.disk.rest.exceptions.ServerException;
import com.yandex.disk.rest.exceptions.ServerIOException;
import com.yandex.disk.rest.exceptions.WrongMethodException;
import com.yandex.disk.rest.json.DiskInfo;
import com.yandex.disk.rest.json.Link;
import com.yandex.disk.rest.json.ResourceList;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws ServerException, IOException {
        RestClient restClient = new RestClient(new Credentials("a05c82f8369842e9bf6ce6757bc830b9", "y0_AgAAAAAkNh3wAAkaugAAAADbpSLFITgfEObsRyGBUOYSI54V2uG-P4g"));
        Link urlFile = restClient.getUploadLink("%disk%text.txt", false);
        restClient.uploadFile(urlFile, true, new File("src/main/java/text.txt"), (ProgressListener) urlFile);
    }
}
